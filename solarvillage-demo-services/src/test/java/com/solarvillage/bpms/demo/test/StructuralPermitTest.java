/**
 * 
 */
package com.solarvillage.bpms.demo.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.solarvillage.bpms.demo.model.StructuralPermitRequest;
import com.solarvillage.bpms.demo.service.StructuralPermitService;
import com.solarvillage.bpms.demo.util.RequestStatus;


@RunWith(Arquillian.class)
public class StructuralPermitTest {
    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackages(true, 
                		Filters.exclude(StructuralPermitTest.class.getPackage()), 
                		"com.solarvillage.bpms.demo")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource("test-ds.xml");
    }
    
    private static final String REQUESTOR = new String("Xicote");

    @Inject
    StructuralPermitService service;

    @Inject
    Logger log;

    @Test
    public void testRegister() throws Exception {
    	log.info("Create structural permit request");
    	StructuralPermitRequest request = new StructuralPermitRequest();
    	request.setRequestor(REQUESTOR);
    	request.setContactInformation("contactInformation");
    	request.setProjectInformation("proyectInformation");
    	request.setStatus(RequestStatus.IN_PROGRESS.toString());
		service.registerRequest(request );
		assertNotNull(request.getId());
        log.info("Structural permit request was persisted with id " + request.getId()
        			+ " - status '" + request.getStatus() + "'.");
    	
    	Long id = request.getId();;
        log.info("Updating structural permit request status to ACCEPTED");
		service.updateStatus(id, RequestStatus.APPROVED.toString());
        request = null;
		request = service.find(id);
    	log.info("Structural permit request status: " + request.getId()
			+ " - '" + request.getStatus() + "'.");
    	assertEquals(request.getStatus(), RequestStatus.APPROVED.toString());
    	
    	log.info("Updating structural permit request status to REJECTED");
    	service.updateStatus(id, RequestStatus.DENIED.toString());
        request = null;
		request = service.getStatus(id);
    	log.info("Structural permit request status: " + request.getId()
			+ " - '" + request.getStatus() + "'.");
    	assertEquals(request.getStatus(), RequestStatus.DENIED.toString());
    	
    	log.info("Deleting structural permit request");
    	service.deleteRequest(id);
		request = null;
		try {
			request = service.find(id);
		}catch (Exception e){
			log.info(e.getMessage());
		}
    	assertEquals(null, request);
    	
    }

}
