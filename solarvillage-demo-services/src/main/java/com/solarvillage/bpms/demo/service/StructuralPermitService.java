package com.solarvillage.bpms.demo.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.solarvillage.bpms.demo.data.StructuralPermitRepository;
import com.solarvillage.bpms.demo.model.StructuralPermitRequest;
import com.solarvillage.bpms.demo.util.RequestStatus;


@Stateless
public class StructuralPermitService {
	
    @Inject
    private Logger log;
    
    @Inject
    StructuralPermitRepository repository;

    public void registerRequest(StructuralPermitRequest request) throws Exception {
        if(request.getRequestor() == null || request.getContactInformation() == null
        		|| request.getProjectInformation() == null) {
        	log.severe("Owner, contact and project information must have a value.");
        	throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        request.setStatus(RequestStatus.IN_PROGRESS.toString());
        repository.register(request);
    }
    
    public StructuralPermitRequest find(Long id) {
    	StructuralPermitRequest request = repository.find(id);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return request;    	
    }

    public StructuralPermitRequest findByRequestor(String requestor) {
    	StructuralPermitRequest request = repository.findByRequestor(requestor);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return request;    	
    }
    
    public StructuralPermitRequest getStatus(Long id) {
    	String status = repository.getstatus(id);

    	StructuralPermitRequest request = new StructuralPermitRequest();
    	request.setId(id);
        request.setStatus(status);

	    return request;
        
    }
    
    public void updateStatus(Long id, String status) {
    	try{
            if (RequestStatus.valueOf(status).equals(RequestStatus.IN_PROGRESS)) {
        		throw new WebApplicationException(
        				new Throwable("New status must be APPROVED or DENIED."), Response.Status.BAD_REQUEST);
            }
    	}catch (IllegalArgumentException e) {
    		throw new WebApplicationException(
    				new Throwable("New status must be APPROVED or DENIED."), Response.Status.BAD_REQUEST);
		}
    	StructuralPermitRequest request = find(id);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        request.setStatus(status);
        repository.updateRequest(request);
        
    }
    
    public void deleteRequest(Long id) {
    	repository.deleteRequest(id);
    }
    
}
