package com.solarvillage.bpms.demo.util;

public enum RequestStatus {
	APPROVED, DENIED, IN_PROGRESS, CANCELLED
}
