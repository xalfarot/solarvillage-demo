package com.solarvillage.bpms.demo.data;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.solarvillage.bpms.demo.model.StructuralPermitRequest;

@ApplicationScoped
public class StructuralPermitRepository {
	
	@Inject
	private EntityManager em;
	
    public void register(StructuralPermitRequest permit) throws Exception {
        em.persist(permit);
    }


    public StructuralPermitRequest find(Long id) {
    	return em.find(StructuralPermitRequest.class, id);
    }

    public StructuralPermitRequest findByRequestor(String requestor) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<StructuralPermitRequest> criteria = cb.createQuery(StructuralPermitRequest.class);
        Root<StructuralPermitRequest> request = criteria.from(StructuralPermitRequest.class);
        criteria.select(request).where(cb.equal(request.get("requestor"), requestor));
        return em.createQuery(criteria).getSingleResult();
    }
    

    public String getstatus(Long id) {
    	CriteriaBuilder cb = em.getCriteriaBuilder();
    	CriteriaQuery<String> criteria = cb.createQuery(String.class);
    	Root<StructuralPermitRequest> request =  criteria.from(StructuralPermitRequest.class);
    	criteria.select(request.<String> get("status")).where(cb.equal(request.get("id"), id));
    	
        return em.createQuery(criteria).getSingleResult();
    }
    
    public void updateRequest(StructuralPermitRequest permit) {
    	em.merge(permit);
    }
    
    public void deleteRequest(Long id) {
    	StructuralPermitRequest request = em.find(StructuralPermitRequest.class, id);
    	em.remove(request);
    }

}
