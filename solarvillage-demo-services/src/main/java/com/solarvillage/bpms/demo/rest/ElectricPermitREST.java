package com.solarvillage.bpms.demo.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.solarvillage.bpms.demo.model.ElectricPermitRequest;
import com.solarvillage.bpms.demo.service.ElectricPermitService;

@Path("/electric")
@RequestScoped
public class ElectricPermitREST {
	
	@Inject
    ElectricPermitService service;

	@Inject
	private Logger log;
	
	@POST
	@Path("/request")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createRequest(ElectricPermitRequest request){
		log.info("REST :: ELC :: Creating a new request: " + request.toString());
		Response.ResponseBuilder builder = null;
		try {
			service.registerRequest(request);
			builder = Response.ok(request, MediaType.APPLICATION_JSON);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}
	
	@GET
	@Path("/request/{id:[0-9][0-9]*}/status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRequestStatus(@PathParam("id") Long id) {
		log.info("REST :: ELC :: Getting request status: " + id);
		Response.ResponseBuilder builder = null;
		try {
			ElectricPermitRequest request = service.getStatus(id);
			builder = Response.ok(request, MediaType.APPLICATION_JSON);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}
	
	
	@PUT
	@Path("/request/{id:[0-9][0-9]*}/status/{status}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRequestStatus(@PathParam("id") Long id, 
			@PathParam("status") String status){
		log.info("REST :: ELC :: Updating request status: " + id + " - " + status);
		Response.ResponseBuilder builder = null;
		try {
			service.updateStatus(id, status);
			builder = Response.ok();
			
		}catch (Exception e) {
			Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}
	
	@DELETE
	@Path("/request/{id:[0-9][0-9]*}")
	public Response deleteRequest(@PathParam("id") Long id) {
		log.info("REST :: ELC :: Deleting request: " + id);
		Response.ResponseBuilder builder = null;
		try {
			service.deleteRequest(id);
			builder = Response.ok();
			
		}catch (Exception e) {
			Map<String, String> responseObj = new HashMap<String, String>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();		
	}
}
