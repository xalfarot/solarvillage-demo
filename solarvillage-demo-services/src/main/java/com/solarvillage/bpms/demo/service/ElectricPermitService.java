package com.solarvillage.bpms.demo.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.solarvillage.bpms.demo.data.ElectricPermitRepository;
import com.solarvillage.bpms.demo.model.ElectricPermitRequest;
import com.solarvillage.bpms.demo.util.RequestStatus;

@Stateless
public class ElectricPermitService {
	
    @Inject
    private Logger log;
    
    @Inject
    ElectricPermitRepository repository;

    public void registerRequest(ElectricPermitRequest request) throws Exception {
        if(request.getRequestor() == null || request.getContactInformation() == null
        		|| request.getProjectInformation() == null) {
        	log.severe("Owner, contact and project information must have a value.");
        	throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        request.setStatus(RequestStatus.IN_PROGRESS.toString());
        repository.register(request);
    }
    
    public ElectricPermitRequest find(Long id) {
    	ElectricPermitRequest request = repository.find(id);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return request;    	
    }

    public ElectricPermitRequest findByOwner(String owner) {
    	ElectricPermitRequest request = repository.findByOwner(owner);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return request;    	
    }
    
    public ElectricPermitRequest getStatus(Long id) {
    	String status = repository.getstatus(id);

        ElectricPermitRequest request = new ElectricPermitRequest();
        request.setId(id);
        request.setStatus(status);

	    return request;
        
    }

    public void updateStatus(Long id, String status) {
    	try{
            if (RequestStatus.valueOf(status).equals(RequestStatus.IN_PROGRESS)) {
        		throw new WebApplicationException(
        				new Throwable("New status must be APPROVED or DENIED."), Response.Status.BAD_REQUEST);
            }
    	}catch (IllegalArgumentException e) {
    		throw new WebApplicationException(
    				new Throwable("New status must be APPROVED or DENIED."), Response.Status.BAD_REQUEST);
		}
    	ElectricPermitRequest request = find(id);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        request.setStatus(status);
        repository.updateRequest(request);
        
    }
    
    public void deleteRequest(Long id) {
    	repository.deleteRequest(id);
    }

}
