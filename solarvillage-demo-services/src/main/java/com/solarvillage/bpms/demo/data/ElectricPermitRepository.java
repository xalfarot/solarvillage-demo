package com.solarvillage.bpms.demo.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.solarvillage.bpms.demo.model.ElectricPermitRequest;

@ApplicationScoped
public class ElectricPermitRepository {
	
	@Inject
	private EntityManager em;
	
	public List<ElectricPermitRequest> getAllElectricalPermits(){
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ElectricPermitRequest> criteria = cb.createQuery(ElectricPermitRequest.class);
		Root<ElectricPermitRequest> permit = criteria.from(ElectricPermitRequest.class);
		criteria.select(permit);
		TypedQuery<ElectricPermitRequest> q = em.createQuery(criteria);
		return q.getResultList();
	}
	
    public void register(ElectricPermitRequest permit) throws Exception {
        em.persist(permit);
    }


    public ElectricPermitRequest find(Long id) {
    	return em.find(ElectricPermitRequest.class, id);
    }

    public ElectricPermitRequest findByOwner(String owner) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ElectricPermitRequest> criteria = cb.createQuery(ElectricPermitRequest.class);
        Root<ElectricPermitRequest> request = criteria.from(ElectricPermitRequest.class);
        criteria.select(request).where(cb.equal(request.get("owner"), owner));
        return em.createQuery(criteria).getSingleResult();
    }
    
    public String getstatus(Long id) {
    	CriteriaBuilder cb = em.getCriteriaBuilder();
    	CriteriaQuery<String> criteria = cb.createQuery(String.class);
    	Root<ElectricPermitRequest> request =  criteria.from(ElectricPermitRequest.class);
    	criteria.select(request.<String> get("status")).where(cb.equal(request.get("id"), id));
    	
        return em.createQuery(criteria).getSingleResult();
    }
    
    public void updateRequest(ElectricPermitRequest permit) {
    	em.merge(permit);
    }
    
    public void deleteRequest(Long id) {
    	ElectricPermitRequest request = em.find(ElectricPermitRequest.class, id);
    	em.remove(request);
    }

}
