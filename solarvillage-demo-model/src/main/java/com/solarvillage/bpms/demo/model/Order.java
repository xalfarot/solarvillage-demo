package com.solarvillage.bpms.demo.model;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6225319852950406905L;
	
	private String requestorName;
	private String requestorContactInformation;
	private Boolean owner;
	private String ownerName;
	private String ownerContactInformation;
	private String projectInformation;
	private Boolean hoa;
	private Date hoaMeetingDate;
	private String hoaApprovementStatus;
	private Long electricalPermit;
	private Long structuralPermit;
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	public String getRequestorContactInformation() {
		return requestorContactInformation;
	}
	public void setRequestorContactInformation(String requestorContactInformation) {
		this.requestorContactInformation = requestorContactInformation;
	}
	public Boolean getOwner() {
		return owner;
	}
	public void setOwner(Boolean owner) {
		this.owner = owner;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOwnerContactInformation() {
		return ownerContactInformation;
	}
	public void setOwnerContactInformation(String ownerContactInformation) {
		this.ownerContactInformation = ownerContactInformation;
	}
	public String getProjectInformation() {
		return projectInformation;
	}
	public void setProjectInformation(String projectInformation) {
		this.projectInformation = projectInformation;
	}
	public Boolean getHoa() {
		return hoa;
	}
	public void setHoa(Boolean hoa) {
		this.hoa = hoa;
	}
	public Date getHoaMeetingDate() {
		return hoaMeetingDate;
	}
	public void setHoaMeetingDate(Date hoaMeetingDate) {
		this.hoaMeetingDate = hoaMeetingDate;
	}
	public String getHoaApprovementStatus() {
		return hoaApprovementStatus;
	}
	public void setHoaApprovementStatus(String hoaApprovementStatus) {
		this.hoaApprovementStatus = hoaApprovementStatus;
	}
	public Long getElectricalPermit() {
		return electricalPermit;
	}
	public void setElectricalPermit(Long electricalPermit) {
		this.electricalPermit = electricalPermit;
	}
	public Long getStructuralPermit() {
		return structuralPermit;
	}
	public void setStructuralPermit(Long structuralPermit) {
		this.structuralPermit = structuralPermit;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Order [requestorName=");
		builder.append(requestorName);
		builder.append(", requestorContactInformation=");
		builder.append(requestorContactInformation);
		builder.append(", owner=");
		builder.append(owner);
		builder.append(", ownerName=");
		builder.append(ownerName);
		builder.append(", ownerContactInformation=");
		builder.append(ownerContactInformation);
		builder.append(", projectInformation=");
		builder.append(projectInformation);
		builder.append(", hoa=");
		builder.append(hoa);
		builder.append(", hoaMeetingDate=");
		builder.append(hoaMeetingDate);
		builder.append(", hoaApprovementStatus=");
		builder.append(hoaApprovementStatus);
		builder.append(", electricalPermit=");
		builder.append(electricalPermit);
		builder.append(", structuralPermit=");
		builder.append(structuralPermit);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
