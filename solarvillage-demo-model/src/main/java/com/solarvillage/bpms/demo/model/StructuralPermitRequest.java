package com.solarvillage.bpms.demo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author xicotencatl
 *
 */
@SuppressWarnings("serial")
@Entity
@Table
public class StructuralPermitRequest implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
	
	@NotNull
    @NotEmpty
    private String requestor;
	
	@NotNull
    @NotEmpty
    private String contactInformation;

	@NotNull
    @NotEmpty
    private String projectInformation;

	@NotNull
    @NotEmpty
    private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(String contactInformation) {
		this.contactInformation = contactInformation;
	}

	public String getProjectInformation() {
		return projectInformation;
	}

	public void setProjectInformation(String projectInformation) {
		this.projectInformation = projectInformation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StructuralPermitRequest [id=");
		builder.append(id);
		builder.append(", requestor=");
		builder.append(requestor);
		builder.append(", contactInformation=");
		builder.append(contactInformation);
		builder.append(", projectInformation=");
		builder.append(projectInformation);
		builder.append(", status=");
		builder.append(status);
		builder.append("]");
		return builder.toString();
	}

	

	   
}
